//Programa: Monitoracao de planta usando Arduino
//Autor: Anderson

//Carrega a biblioteca LiquidCrystal
#include <LiquidCrystal.h>

#include <DHT.h>
 
#define pino_led_vermelho 8
#define pino_led_amarelo 9
#define pino_led_verde 10

//#define DHTPIN 7
//#define DHTTYPE DHT11
DHT dht(7, DHT11);

//Define os pinos que serão utilizados para ligação ao display
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

byte smile[8] = 
              {
                0b00000,
                0b00000,
                0b01010,
                0b00000,
                0b10001,
                0b01110,
                0b00000,
                0b00000
              };
byte sad[8] = 
              {
                0b00000,
                0b00000,
                0b01010,
                0b00000,
                0b00000,
                0b01110,
                0b10001,
                0b00000
              };

byte normal[8] = 
              {
                0b00000,
                0b00000,
                0b01010,
                0b00000,
                0b00000,
                0b11111,
                0b00000,
                0b00000
              };
 
void setup()
{
  Serial.begin(9600);
  //Define o número de colunas e linhas do LCD
  lcd.begin(16,2);
  lcd.createChar(0, smile);
  lcd.createChar(1, normal);
  lcd.createChar(2, sad);

  lcd.home(); // Go home on LCD
  lcd.print("Lino a planta");
  delay(3000);
  dht.begin();
    
  pinMode(A2, INPUT);
  pinMode(pino_led_vermelho, OUTPUT);
  pinMode(pino_led_amarelo, OUTPUT);
  pinMode(pino_led_verde, OUTPUT);
}
 
void loop()
{
  delay(2000);
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  if (isnan(h) || isnan(t)) 
  {
   lcd.clear();
    lcd.setCursor (0,0);
    Serial.print("Failed to read from DHT sensor");
    return;
  }
  
  //Limpa a tela
  lcd.clear();
  
  //Le o valor do pino A0 do sensor
  int valor_analogico = analogRead(A2);
 
  //Mostra o valor da porta analogica no serial monitor
  Serial.print("Porta analogica: ");
  Serial.print(valor_analogico);
  Serial.println("");
 
  //Solo umido, acende o led amarelo
  if (valor_analogico > 0 && valor_analogico < 400)
  {
    Serial.println(" Status: Umidade alta");
    apagaleds();
    digitalWrite(pino_led_amarelo, HIGH);
    
    lcd.clear();
    
    lcd.setCursor(0,0);
      lcd.print("U:");lcd.print(h);lcd.print("%");lcd.print(" T:");lcd.print(t);
    lcd.setCursor(0,1); 
    
    //lcd.setCursor(0,1);
      lcd.print("Lino - Indo ");
    //lcd.setCursor(0,1);
      for (int i=0; i <=3; i++)
      {
        lcd.write(byte(1));
      } 
    delay(300);
  }
 
  //Solo com umidade moderada, acende led verde
  if (valor_analogico > 400 && valor_analogico < 800)
  {
    Serial.println(" Status: Umidade moderada");
    apagaleds();
    digitalWrite(pino_led_verde, HIGH);

    lcd.clear();
    
    lcd.setCursor(0,0);
      lcd.print("U:");lcd.print(h);lcd.print("%");lcd.print(" T:");lcd.print(t);
    lcd.setCursor(0,1); 
    
    //lcd.setCursor(0,1);
      lcd.print("Lino -  OK  ");
    //lcd.setCursor(0,1);
      for (int i=0; i <=3; i++)
      {
        lcd.write(byte(0));
      } 
    delay(300);
    /*lcd.clear();
    lcd.print("Lino - OK");
    lcd.setCursor(0,1);
    for (int i=0; i <=6; i++)
    {
      lcd.write(byte(0));
    } 
    delay(300);*/
    
  }
 
  //Solo seco, acende led vermelho
  if (valor_analogico > 800 && valor_analogico < 1024)
  {
    Serial.println(" Status: Solo seco");
    apagaleds();
    digitalWrite(pino_led_vermelho, HIGH);

    lcd.clear();
    
    lcd.setCursor(0,0);
      lcd.print("U:");lcd.print(h);lcd.print("%");lcd.print(" T:");lcd.print(t);/*lcd.print((char)223);lcd.print("C ");*/
    lcd.setCursor(0,1); 
    
    //lcd.setCursor(0,1);
      lcd.print("Lino - Seco ");
    //lcd.setCursor(0,1);
      for (int i=0; i <=3; i++)
      {
        lcd.write(byte(2));
      } 
    delay(300);
    /*lcd.clear();
    lcd.print("Lino - seco");
    lcd.setCursor(0,1);
    for (int i=0; i <=6; i++)
    {
      lcd.write(byte(2));
    } 
    delay(300);*/
  }
  delay(1000);
}
 
void apagaleds()
{
  digitalWrite(pino_led_vermelho, LOW);
  digitalWrite(pino_led_amarelo, LOW);
  digitalWrite(pino_led_verde, LOW);
}

/*
byte white[8] = 
              {
                0b00000,
                0b00000,
                0b00000,
                0b00000,
                0b00000,
                0b00000,
                0b00000,
                0b00001
              };
byte eye[8] = 
              {
                0b11111,
                0b11111,
                0b11111,
                0b11111,
                0b10001,
                0b10001,
                0b10001,
                0b11111
              };

byte lftsmile[8] = 
              {
                0b10000,
                0b11000,
                0b01100,
                0b00110,
                0b00011,
                0b00001,
                0b00000,
                0b00000
              };

byte rgtsmile[8] = 
              {
                0b00001,
                0b00011,
                0b00110,
                0b01100,
                0b11000,
                0b10000,
                0b00000,
                0b00000
              };

byte grnsmile[8] = 
              {
                0b00000,
                0b00000,
                0b00000,
                0b00000,
                0b11111,
                0b11111,
                0b00000,
                0b00000
              };

byte ylwsmile[8] = 
              {
                0b00000,
                0b00000,
                0b00000,
                0b11111,
                0b11111,
                0b00000,
                0b00000,
                0b00000
              };

byte lftsad[8] = 
              {
                0b00000,
                0b00000,
                0b00001,
                0b00011,
                0b00110,
                0b01100,
                0b11000,
                0b10000
              };

byte rgtsad[8] = 
              {
                0b00000,
                0b00000,
                0b10000,
                0b11000,
                0b01100,
                0b00110,
                0b00011,
                0b00001
              };

byte redsmile[8] = 
              {
                0b00000,
                0b00000,
                0b11111,
                0b11111,
                0b00000,
                0b00000,
                0b00000,
                0b00000
              };
              
void setup()
{
  //Define o número de colunas e linhas do LCD
  lcd.begin(16,2);
  //lcd.createChar(1, white);
  //lcd.createChar(2, eye);
  //lcd.createChar(3, lftsmile);
  //lcd.createChar(4, rgtsmile);
  //lcd.createChar(5, grnsmile);
  //lcd.createChar(6, ylwsmile);
  //lcd.createChar(7, lftsad);
  //lcd.createChar(8, rgtsad);
  //lcd.createChar(9, redsmile);
    
  Serial.begin(9600);
  pinMode(A2, INPUT);
  pinMode(pino_led_vermelho, OUTPUT);
  pinMode(pino_led_amarelo, OUTPUT);
  pinMode(pino_led_verde, OUTPUT);
}
 
void loop()
{
  //Limpa a tela
  lcd.clear();
  
  //Le o valor do pino A0 do sensor
  int valor_analogico = analogRead(A2);
 
  //Mostra o valor da porta analogica no serial monitor
  Serial.print("Porta analogica: ");
  Serial.print(valor_analogico);
  Serial.println("");
 
  //Solo umido, acende o led amarelo
  if (valor_analogico > 0 && valor_analogico < 400)
  {
    Serial.println(" Status: Solo umido");
    apagaleds();
    digitalWrite(pino_led_amarelo, HIGH);
    
    lcd.clear();
    //lcd.print("Lino - Legal");
    lcd.setCursor(0,0);
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(1));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(1));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0)); 
    
    lcd.setCursor(0,1);
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(5));
    lcd.write(byte(5));
    lcd.write(byte(5));
    lcd.write(byte(5));
    lcd.write(byte(5));
    lcd.write(byte(5));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    delay(300);
  }
 
  //Solo com umidade moderada, acende led verde
  if (valor_analogico > 400 && valor_analogico < 800)
  {
    Serial.println(" Status: Umidade moderada");
    apagaleds();
    digitalWrite(pino_led_verde, HIGH);

    lcd.clear();
    lcd.setCursor(0,0);
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(1));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(1));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0)); 
    
    lcd.setCursor(0,1);
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(2));
    lcd.write(byte(4));
    lcd.write(byte(4));
    lcd.write(byte(4));
    lcd.write(byte(4));
    lcd.write(byte(4));
    lcd.write(byte(4));
    lcd.write(byte(3));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    delay(300);
    
  }
 
  //Solo seco, acende led vermelho
  if (valor_analogico > 800 && valor_analogico < 1024)
  {
    Serial.println(" Status: Solo seco");
    apagaleds();
    digitalWrite(pino_led_vermelho, HIGH);

    lcd.clear();
    lcd.setCursor(0,0);
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(1));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(1));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0)); 
    
    //lcd.setCursor(1,1);
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(6));
    lcd.write(byte(8));
    lcd.write(byte(8));
    lcd.write(byte(8));
    lcd.write(byte(8));
    lcd.write(byte(8));
    lcd.write(byte(8));
    lcd.write(byte(7));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    lcd.write(byte(0));
    delay(300);
  }
  delay(1000);
}
 
void apagaleds()
{
  digitalWrite(pino_led_vermelho, LOW);
  digitalWrite(pino_led_amarelo, LOW);
  digitalWrite(pino_led_verde, LOW);
}
*/
